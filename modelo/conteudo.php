<?php
    class Conteudo{
        private $id;
        private $nome;
        private $titulo;
        private $subti;
        private $cont;
        private $autor;
        private $tipo;

        
        
        public function getId(){
            return $this->id;
        }
        public function getNome(){
            return $this->nome;
        }
        public function getTitulo(){
            return $this->titulo;
        }
        public function getSubti(){
            return $this->subti;
        }
        public function getCont(){
            return $this->cont;
        }
        public function getAutor(){
            return $this->autor;
        }
        public function getTipo(){
            return $this->tipo;
        }
    
        public function setId($i){
            $this->id = $i; 
        }
        public function setNome($n){
            $this->nome = $n;
        }
        public function setTitulo($t){
            $this->titulo = $t;
        }
        public function setSubti($s){
            $this->subti = $s;
        }
        public function setCont($c){
            $this->cont = $c;
        }
        public function setAutor($a){
            $this->autor = $a;
        }
        public function setTipo($p){
            $this->tipo = $p;
        }
       
    }
?>  
