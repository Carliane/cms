<?php
require_once("Conexao.php");
require_once("../modelo/usuario.php");
    class UsuarioControle{
        function remover($id){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("DELETE FROM user WHERE id=:id");
                $cmd->bindParam("id", $id);
                if($cmd->execute()){
                    return true;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro de PDO: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function selecionarTodos(){
            try{
                $conexao = new Conexao();
                $cmd = $conexao->getConexao()->prepare("SELECT * FROM user;");
                $cmd->execute();
                $resultado = $cmd->fetchAll(PDO::FETCH_CLASS, "usuario");
                return $resultado;
            }catch(PDOException $e){
                echo "Erro no banco: {$e->getMessage()}";
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
            }
        }
        function inserir($usuario){
            try{
                $conexao = new Conexao();
                $nome = $usuario->getNome();
                $user = $usuario->getUser();
                $mail = $usuario->getEmail();
                $senha = $usuario->getSenha();
               
                $cmd = $conexao->getConexao()->prepare("INSERT INTO user(nome,user,email,senha) VALUES(:n,:u,:e,:s);");
                $cmd->bindParam("n", $nome);
                $cmd->bindParam("u", $user);
                $cmd->bindParam("e", $mail);
                $cmd->bindParam("s", $senha);
                if($cmd->execute()){
                    $conexao->fecharConexao();
                    return true;
                }else{
                    $conexao->fecharConexao();
                    return false;
                }
            }catch(PDOException $e){
                echo "Erro do banco: {$e->getMessage()}";
                return false;
            }catch(Exception $e){
                echo "Erro geral: {$e->getMessage()}";
                return false;
            }
        }
        function update( $usuario ) {
       try {
                 $conexao = new Conexao();
         

                $nome = $usuario->getNome();
                $user = $usuario->getUser();
                $mail = $usuario->getEmail();
                $senha = $usuario->getSenha();
       $cmd= $conexao->getConexao()->prepare("UPDATE user SET NOME=:nome, EMAIL=:email, TELEFONE=:telefone WHERE ID=:id");
                 $cmd->bindParam("n", $nome);
                $cmd->bindParam("u", $user);
                $cmd->bindParam("e", $mail);
                $cmd->bindParam("s", $senha);
          if($cmd->execute()){
             if($cmd->rowCount() > 0){
                return true;
             } else {
                return false;
             }
          } else {
             return false;
          }
       } catch ( PDOException $excecao ) {
          echo $excecao->getMessage();
       }
    }

  
    
}
?>





