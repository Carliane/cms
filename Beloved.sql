DROP DATABASE IF EXISTS beloved; 
CREATE DATABASE IF NOT EXISTS beloved;

CREATE TABLE beloved.user(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100) NOT NULL,
	user VARCHAR(100) NOT NULL,
	email VARCHAR(200) NOT NULL,
	senha VARCHAR(80) NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE beloved.conteudo(
	id INT(3) NOT NULL AUTO_INCREMENT,
	nome VARCHAR(255) NOT NULL,
	tipo VARCHAR(255) NOT NULL,
	titulo VARCHAR(255) NOT NULL,
	subti VARCHAR(255) NOT NULL,
	autor VARCHAR(255) NOT NULL,
	cont VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
	);

CREATE TABLE beloved.logo(
	id INT(3) AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	tipo VARCHAR(255) NOT NULL,
	bin LONGBLOB NOT NULL,
	PRIMARY KEY(id)

);
CREATE TABLE beloved.img(
	id INT(3) AUTO_INCREMENT NOT NULL,
	no VARCHAR(255) NOT NULL,
	ti VARCHAR(255) NOT NULL,
	bob LONGBLOB NOT NULL,
	PRIMARY KEY(id)

);
CREATE TABLE beloved.cont(
	id INT(3) AUTO_INCREMENT NOT NULL,
	titulo VARCHAR(255) NOT NULL,
	conteudo VARCHAR(255) NOT NULL,
	autor VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)

);
CREATE TABLE beloved.banner(
	id INT(3) AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	tipo VARCHAR(255) NOT NULL,
	bin LONGBLOB NOT NULL,
	PRIMARY KEY(id)

);
      

INSERT INTO beloved.user(user, senha, email, nome) Values("admin", "admin","1","admin");
INSERT INTO beloved.conteudo(nome,tipo,titulo,subti,cont,autor) VALUES("Bird", "foto", "foto ai","um teste para criacao","muitoaaaaaaaaaaaaaaaaaaaa coisa e importante","eu" );
